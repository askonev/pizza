import json


class StorageManager:
    def load_data(self, data_name: str) -> dict:
        try:
            with open(self.get_filename(data_name), 'r') as file:
                data = json.loads(file.read())
        except FileNotFoundError:
            data = {}
        return data

    def save_data(self, data_name, data_content):
        with open(self.get_filename(data_name), 'w') as file:
            file.write(json.dumps(data_content))

    @staticmethod
    def get_filename(data_name):
        return data_name + ".json"
