import json

from kivy.network.urlrequest import UrlRequest


class HttpClient:
    @staticmethod
    def get_pizzas(on_receive, on_server_error, on_data_failure):
        url = "https://jrpizzamamadjango3.herokuapp.com/api/GetPizzass"

        def data_received(req, result):
            if on_receive:
                on_receive((
                    item['fields']
                    for item in json.loads(result)
                ))

        def data_error(req, error):
            if on_server_error:
                on_server_error(error)

        def data_failure(req, status):
            if on_data_failure:
                on_data_failure("Status: " + str(req.resp_status))

        UrlRequest(
            url,
            on_success=data_received,
            on_error=data_error,
            on_failure=data_failure
        )
