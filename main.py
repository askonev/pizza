from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, NumericProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.behaviors import CoverBehavior

from http_client import HttpClient
from models import Pizza
from storage_manager import StorageManager


class PizzaWidget(BoxLayout):
    name = StringProperty()
    ingredients = StringProperty()
    price = NumericProperty()
    vegetarian = BooleanProperty()


class MainWidget(FloatLayout):
    recycleView = ObjectProperty(None)
    error_str = StringProperty("")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        HttpClient().get_pizzas(self.fill_data, self.on_server_error, self.on_data_failure)

    def on_parent(self, widget, parent):
        self.recycleView.data = StorageManager().load_data("pizzas")

    def fill_data(self, data):
        self.recycleView.data = data
        StorageManager().save_data("pizzas", data)

    def on_server_error(self, error):
        self.error_str = "ERROR " + str(error)

    def on_data_failure(self, status):
        self.error_str = "FAILURE " + str(status)


with open("pizzascr.kv", encoding='utf-8') as f:
    Builder.load_string(f.read())


class PizzaApp(App):
    def build(self):
        return MainWidget()


PizzaApp().run()
